<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockDatum extends Model {

    /**
     * Generated
     */

    protected $table = 'stock_data';
    protected $fillable = ['id', 'stockNumber', 'recordedDate', 'tradedShares', 'tradedPayment', 'openingPrice', 'highPrice', 'lowPrice', 'closingPrice', 'priceDiff', 'volume'];


    public function stock() {
        return $this->belongsTo(\App\Models\Stock::class, 'stockNumber', 'stockNumber');
    }


}
