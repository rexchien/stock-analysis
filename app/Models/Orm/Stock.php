<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model {

    /**
     * Generated
     */

    protected $table = 'stocks';
    protected $fillable = ['id', 'stockNumber', 'stockName'];


    public function stockData() {
        return $this->hasMany(\App\Models\StockDatum::class, 'stockNumber', 'stockNumber');
    }


}
