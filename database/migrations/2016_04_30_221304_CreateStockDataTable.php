<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stockNumber')->comment('股票代碼');
            $table->date('recordedDate')->comment('紀錄日期');
            $table->integer('tradedShares')->comment('成交股數');
            $table->integer('tradedPayment')->comment('成交金額');
            $table->decimal('openingPrice', 5, 2)->comment('開盤價');
            $table->decimal('highPrice', 5, 2)->comment('最高價');
            $table->decimal('lowPrice', 5, 2)->comment('最低價');
            $table->decimal('closingPrice', 5, 2)->comment('收盤價');
            $table->decimal('priceDiff', 5, 2)->comment('漲跌價差');
            $table->integer('volume')->comment('成交筆數');
            $table->timestamps();
            
            $table->foreign('stockNumber')->reference('stockNumber')->on('stocks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stock_data');
    }
}
